%% Test for estimator_A
%  Estimation of a weak probability
%
% Authors
%
% * Implementation: Teddy Furon
% * Science: Frederic Cerou, Pierre Del Moral, Arnaud Guyader, Teddy Furon
%
% Contact: teddy.furon@inria.fr
%
% Copyright INRIA 2010-2011
%
% Licence 
% This software and its subprograms are governed by the CeCILL license 
% under French law and abiding by the rules of distribution of free software. 
% See http://www.cecill.info/licences.en.html

%% Example

%%
% What is the probability that
%
% * *x* a white gaussian noise in R^20
% * lies in the hypercone of axis (1,0,...,0) and angle acos(0.95)

%% Data
dim = 20;
threshold = 0.95;
n = 2000; % Total number of particles
k = 1000; % number of survivors

% real value for this problem
Proba_true = 1 - betainc(threshold^2,1/2,(dim-1)/2);

%% Prediction of the Estimator performance
Stat_pre = stats_estimator_A(n,k,'P',Proba_true)


%% Estimation
[Proba_est, Stat, Intern] = estimator_A(threshold,dim,'n',n,'k',k);

%% Display
% Print the true probability
fprintf('\nTrue probability \t %6.2e\n\n',Proba_true);

%%
%
% Print the estimation and its confidence interval.
% This interval only in the asymptotic regime
fprintf('Lower bnd\t\tEstimate \t\tUpper bnd\n');
fprintf('%6.2e\t\t%6.2e \t\t%6.2e\n',Stat.interval(1),Proba_est,Stat.interval(2));

%%
%
% Show the last intermediate thresholds and estimated probability.
figure;
semilogy([Intern(end-10:end-1,2);threshold],[Intern(end-10:end-1,3);Proba_est],'-+');%,,'+');
hold on
semilogy(threshold*[1,1],[Stat.interval(1),Stat.interval(2)],'o-r');
semilogy(threshold,Proba_true,'^g')
legend('Estimate','Confidence','True')
grid on;
v = axis; v(2) =v(2)+0.01;
axis(v);
hold off
title('Test Estimator A')
xlabel('Threshold');
ylabel('Estimated');

