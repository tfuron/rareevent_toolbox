%% Test for estimator_quantile_B
%  Estimation of a large quantile
%
% Authors
%
% * Implementation: Teddy Furon
% * Science: Arnaud Guyader, Nicolas Hengartner, Eric Matzner-Lober
%
% Contact: teddy.furon@inria.fr
%
% Copyright INRIA 2010-2011
%
% Licence
% This software and its subprograms are governed by the CeCILL license 
% under French law and abiding by the rules of distribution of free software. 
% See http://www.cecill.info/licences.en.html

%% Example

%%
% What is the quantile q such that
%
% * *x* a white gaussian noise in R^20
% * lies in the hypercone of axis (1,0,...,0) and angle acos(q)
% * with a probability of 4.70*10^-11

%% Data
dim = 20;
Proba = 4.7*10^-11;
n = 200; % Total number of particles

% real value for this problem
q_true = 0.95;

%% Estimation
[q_est, Stat, Intern] = estimator_quantile_B(Proba,dim,'n',n,'N_sample',200);

%% Display
% Print the true probability
fprintf('\nTrue quantile \t %6.2e\n\n',q_true);

%%
%
% Print the estimation and its confidence interval.
% This interval only in the asymptotic regime
fprintf('Lower bnd\t\tEstimate\t\tUpper bnd\n');
fprintf('%6.2e\t\t%6.2e\t\t%6.2e\n',Stat.interval(1),q_est,Stat.interval(2));

%%
%
% Show the last intermediate thresholds and estimated probability.
figure;
semilogx([Intern(end-10:end-1,3);Proba],[Intern(end-10:end-1,2);q_est],'-+');
hold on
semilogx(Proba*[1,1],[Stat.interval(1),Stat.interval(2)],'o-r');
semilogy(Proba,q_true,'^g')
legend('Estimate','Confidence','True')
grid on;
%v = axis; v(2) =v(2)+0.01;
%axis(v);
hold off
title('Test Estimator B')
xlabel('Probability');
ylabel('Estimated quantile');
