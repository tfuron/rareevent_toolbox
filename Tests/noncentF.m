function p = noncentF(v1,v2,d,F,N)

% Computes the cdf Pr(0<f<F) (F can be a column vector) when f has an
% F density with v1 and v2 degrees of freedom and noncentrality
% parameter d. Uses the expansion in terms of the incomplete Beta
% function up to N terms. The series converges quite quickly.
% function p = noncentF(v1,v2,d,F,N)

% Peyman Milanfar <milanfar@unix.sri.com> Oct. 1995

p=zeros(size(F));

x = v1*F./(v1*F+v2);
b = v2/2;

for i = 0:N-1
   a = (v1/2)+i;
   s = exp(-d/2)*(((d/2)^i)/gamma(i+1))*betainc(x,a,b);
   p = p+s;
end