%% Test for estimator_B
%  Estimation of a weak probability
%
% Authors
%
% * Implementation: Teddy Furon
% * Science: Arnaud Guyader, Nicolas Hengartner, Eric Matzner-Lober
%
% Contact: teddy.furon@inria.fr
%
% Copyright INRIA 2010-2011
%
% Licence
% This software and its subprograms are governed by the CeCILL license 
% under French law and abiding by the rules of distribution of free software. 
% See http://www.cecill.info/licences.en.html

%% Example

%%
% What is the probability that
%
% * *x* a white gaussian noise in R^20
% * lies in the hypercone of axis (1,0,...,0) and angle acos(0.95)

%% Data
dim = 20;
threshold = 0.95;
n = 200; % Total number of particles
t = 30;
% real value for this problem
Proba_true = 1 - betainc(threshold^2,1/2,(dim-1)/2);

%% Prediction of the Estimator performance
Stat_pre = stats_estimator_B(n,'P',Proba_true)


%% Estimation
[Proba_est, Stat, Intern] = estimator_B(threshold,dim,'n',n,'N_sample',200,'t',t);
Lower = Stat.interval(1);
Upper = Stat.interval(2);

%% Display
% Print the true probability
fprintf('\nTrue probability \t %6.2e\n\n',Proba_true);

%%
%
% Print the estimation and its confidence interval.
% This interval only in the asymptotic regime
fprintf('Lower bnd\t\tEstimate\t\tUpper bnd\n');
fprintf('%6.2e\t\t%6.2e\t\t%6.2e\n',Lower,Proba_est,Upper);

%%
%
% Show the last intermediate thresholds and estimated probability.
figure;
subplot(1,2,1)
semilogy([Intern(end-10:end-1,2);threshold],[Intern(end-10:end-1,3);Proba_est],'-+');%,,'+');
hold on
semilogy(threshold*[1,1],[Lower,Upper],'o-r');
semilogy(threshold,Proba_true,'^g')
legend('Estimate','Confidence','True')
grid on;
v = axis; v(2) =v(2)+0.01;
axis(v);
hold off
title('Test Estimator B')
xlabel('Threshold');
ylabel('Estimated');

subplot(1,2,2)
x = linspace(1/2*Stat.interval(1),2*Stat.interval(2),100);
mu = -n*log(Proba_est)*log(1-1/n);
sig2 = -n*log(Proba_est)*log(1-1/n)^2;
pdf_estimator = @(x,m,s2) exp(-(log(x)-m).^2/2/s2)./x/sqrt(2*pi*s2);
%PDF of the estimator - log normal
plot(x,pdf_estimator(x,mu,sig2)); hold on;
x = [Proba_true,Stat.interval(1),Stat.interval(2),Proba_est];
y = pdf_estimator(x,mu,sig2);
stem(x(1),y(1),'^g')
stem(x(2:3),y(2:3),'r');
stem(x(4),y(4),'+')
hold off
xlabel('probability')
ylabel('pdf')
title('pdf of the estimator')
hold off



