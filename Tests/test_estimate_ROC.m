%% Test of the estimate_ROC.m
%  How to plot a Receiver Operating Curve for the hypercone problem ?
%
% Authors
%
% * Implementation: Teddy Furon
% * Science: Arnaud Guyader, Nicolas Hengartner, Eric Matzner-Lober
%
% Contact: teddy.furon@inria.fr
%
% Copyright INRIA 2010-2011
%
% Licence
% This software and its subprograms are governed by the CeCILL license 
% under French law and abiding by the rules of distribution of free software. 
% See http://www.cecill.info/licences.en.html

%% Example

%%
% What is the ROC curve for the following problem:
%
% * The detection region is an hypercone of axis *u* = (1,0,...,0)
% * *x* a white gaussian noise in R^20.
% * H0: The detector receives *x*.
% A false positive occurs when *x* is in the detection region.
% * H1: The detector receives *y* = *x*+ sigw *u* (watermarked signal).
% A false negative occurs when *y* is not in the detection region.

%% General Data
dim = 1024; % dimension of the vector
sigw = 0.1; % watermark power
% Draw a ROC for P_fp from 10^-1 to 10^-7
P_fp_target = logspace(-1,-7,10);



%% Setups for hypotheses H0 and H1
% There are defined by default in estimate_ROC.m

%% ROC estimator
%  Estimate couples (P_fp , P_fn) by varying the angle of the hypercone
[P_fp_est,P_fn_est,Stat] = estimate_ROC(P_fp_target,dim);


%% Ground truth
P_fp_true = 1 - betainc(Stat.threshold.^2,1/2,(dim-1)/2);
% CDF of a central  F-distribution
P_fn_true = noncentF(1,dim-1,dim*sigw^2,...
    (dim-1)*Stat.threshold.^2./(1-Stat.threshold.^2),100);
% CDF of a noncentral F-distribution

%% Draw curve for H0
figure(1);
% The estimated ROC
loglog(P_fp_est,P_fn_est);
hold on
grid on;
xlabel('P_{fp}');
ylabel('P_{fn}');
title('ROC');
% The confidence boxes
IC_fp = Stat.interval_fp;
IC_fn = Stat.interval_fn;
for k=1:length(P_fp_est)
    diffx = (IC_fp(2,k)-IC_fp(1,k));
    x = (IC_fp(1,k)+IC_fp(2,k))/2 - diffx/2;
    diffy = (IC_fn(2,k)-IC_fn(1,k));
    y = (IC_fn(1,k)+IC_fn(2,k))/2 - diffy/2;
   rectangle('Position',[x y diffx diffy]) 
end
% The confidence 'tube'
X = [IC_fp(2,:),fliplr(IC_fp(1,:))];
Y = [IC_fn(2,:),fliplr(IC_fn(1,:))];
h = patch(X,Y,[127 255 212]/255);
set(h,'facealpha',0.8);
set(h,'EdgeColor','none')

% The true probability
plot(P_fp_true,P_fn_true,'-r')
legend('Estimated','Tube','True','Location','SouthWest')
hold off;

