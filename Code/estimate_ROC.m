function [P_fp_est,P_fn_est,Stat] = estimate_ROC(P_fp_target,dim,varargin)
%[P_fp_est,P_fn_est,Stat] = estimate_ROC(P_fp_target,dim,option)
%
% Inputs
% * dim: Dimension of the particule space
% * P_fp_target: Required false positive probabilities
% * option: a structure containing the following parameters (fields)
%
% Parameters
% * H0_setup: a structure defining the setup for H0 simulation
%       compliant with the estimator_B option structure
% * H1_setup: a structure defining the setup for H1 simulation
%       compliant with the estimator_B option structure
%
% Outputs
% * P_fp_est: estimated false positivie probabilities
% * P_fn_est: estimated false positivie probabilities
% * Stat : a structure containing some statistics about the estimation
%
% Comments
% Additional optional fields in H0_setup
% * cell: when a particule is an object coded in a cell
% Additional optional fields in H1_setup
% * cell: when a particule is an object coded in a cell
% * MC: estimate probabilities with a basic Monte Carlo
%
% Authors
% * Implementation: Teddy Furon
% * Science: Frederic Cerou, Pierre Del Moral, Arnaud Guyader, Teddy Furon
%
% Contact: teddy.furon@inria.fr
%
% Copyright INRIA 2010-2011
%
% Licence
% This software and its subprograms are governed by the CeCILL license
% under French law and abiding by the rules of distribution of free software.
% See http://www.cecill.info/licences.en.html

%% Parsing the arguments
error(nargoutchk(0,3,nargout));

% Default setups
H0_default.verbose = false;
H0_default.n = 150;
H0_default.cell = false;

H1_default.verbose = false;
H1_default.n = 300;
H1_default.cell = false;
sigw = 0.1; % watermark power
H1_default.SCORE = @(x)SCORE_H1(x,sigw);

% Create an instance of the inputParser class.
p = inputParser;
% Define inputs that one must pass on every call:
p.addRequired('dim', @(x)validateattributes(x,{'numeric'},...
    {'integer', 'positive','scalar'}));
p.addRequired('P_fp_target',@(x)validateattributes(x,{'numeric'},...
    {'real', 'nonempty','positive'}));

p.addParamValue('H1_setup',H1_default,@(x)isa(x,'struct'));
p.addParamValue('H0_setup',H0_default,@(x)isa(x,'struct'));

% Parse and validate all input arguments.
p.StructExpand = true;
p.KeepUnmatched = true;
p.parse(dim,P_fp_target,varargin{:});

h = p.Results;

%% Hypothesis H0
%  Estimate thresholds for some false positive probabilties
if ~isfield(h.H0_setup,'cell')
    h.H0_setup.cell=false;
end
if h.H0_setup.cell==true % Are you working with cells?
    % Warm up: For required P_fp, we first estimate the associated thresholds
    q= estimator_quantile_B_cell(h.P_fp_target,h.dim,h.H0_setup);
    % Now, for these thresholds, we estimate P_fp
    [P_fp_est,Stat_fp,Intern_fp] = estimator_B_cell(q,h.dim,h.H0_setup);
else
    % Warm up: For required P_fp, we first estimate the associated thresholds
    q= estimator_quantile_B(h.P_fp_target,h.dim,h.H0_setup);
    % Now, for these thresholds, we estimate P_fp
    [P_fp_est,Stat_fp,Intern_fp] = estimator_B(q,h.dim,h.H0_setup);
end


%% Hypothesis H1
% Estimate probabilities for some thresholds
if ~isfield(h.H1_setup,'cell')
    h.H1_setup.cell=false;
end
if ~isfield(h.H1_setup,'MC')
    h.H1_setup.MC=false;
end
if h.H1_setup.MC==true % Do a basic Monte-Carlo
    % because the proba under H1 are not so low
    [P_fn_est,Stat_fn] = estimator_MC(-q,dim,h.H1_setup);
elseif h.H1_setup.cell==true % Are you working with cells?
    [P_fn_est,Stat_fn,Intern_fn] = estimator_B_cell(-q,dim,h.H1_setup);
else
    [P_fn_est,Stat_fn,Intern_fn] = estimator_B(-q,dim,h.H1_setup);
end
%% Build Stat
Stat.interval_fp = [Stat_fp.interval];
Stat.interval_fn = [Stat_fn.interval];
Stat.threshold = q;

%% Subfunctions
%  Default functions
function y = SCORE_H1(X,sigw)
X(1,:) = X(1,:) + sigw*sqrt(size(X,1));
y = - abs(X(1,:))./sqrt(sum(X.^2,1));
