function [Proba, Stat] = estimator_MC(threshold,dim,varargin)
%[Proba, Stat] = estimator_MC(threshold,dim,options)
%
% Inputs
% * threshold : defines the acceptance region score(x)>threshold
% * dim : Dimension of the particule space
% * option: a structure containing the following parameters (fields)
%
% Parameters
% * GENERATE: particule generator          [@(x)GENERATE_INTERNAL]
% * SCORE: particule score                 [@(x)SCORE_INTERNAL]
% * n: total number of particules          [100*dim]
% * verbose                                [true]
%
% Outputs
% * Proba: Estimated Probability
% * Stat : a structure containing some statistics about the estimation
%
% Comments
% * A basic Monte-Carlo estimator
% * can cope with cell (GENERATE, SCORE, MODIFY must deal with cell then)
%
% Authors
% * Implementation: Teddy Furon
% * Science: Arnaud Guyader, Nicolas Hengartner, Eric Matzner-Lober
%
% Contact: teddy.furon@inria.fr
%
% Copyright INRIA 2010-2011
%
% Licence
% This software and its subprograms are governed by the CeCILL license
% under French law and abiding by the rules of distribution of free software.
% See http://www.cecill.info/licences.en.html

%% Parsing the arguments
error(nargoutchk(0,3,nargout));

% Create an instance of the inputParser class.
p = inputParser;
% Define inputs that one must pass on every call:
p.addRequired('threshold',@(x)validateattributes(x,{'numeric'},...
  {'real', 'nonempty','finite'}));
p.addRequired('dim', @(x)validateattributes(x,{'numeric'},...
  {'integer', 'positive','scalar'}));

% Options
p.addParamValue('GENERATE',@GENERATE_INTERNAL,@(x)isa(x,'function_handle'));
p.addParamValue('SCORE',@SCORE_INTERNAL,@(x)isa(x,'function_handle'));
p.addParamValue('verbose',true,@(x)islogical(x));
p.addParamValue('n',10^5,@(x)validateattributes(x,{'numeric'},...
  {'scalar','integer', 'positive'}));

% Parse and validate all input arguments.
p.StructExpand = true;
p.KeepUnmatched = true;
p.parse(threshold,dim,varargin{:});

h = p.Results;

% Display the names of all arguments.
if h.verbose
  fprintf('\n')
  disp 'List of all arguments of estimator_MC'
  disp(p.Results)
end

%% Declare some variables
%threshold = max(h.threshold);
[target_thres_vec, I] = sort(h.threshold);
Proba  = zeros(size(h.threshold));
if nargout == 2
  % Create the structure
  Stat(length(h.threshold),1).rel_var = []; % relative variance of the estimation
  Stat(length(h.threshold),1).rel_bias = [];% relative bias of the estimation
  Stat(length(h.threshold),1).interval = [];% Interval of 95% confidence
  Stat(length(h.threshold),1).NbIter = [];; % Nb of iterations
  Stat = orderfields(Stat);
end
%% Initial step
X = h.GENERATE(h.dim,h.n); % matrix of generated vectors
dx =  h.SCORE(X); % calculate their score

dx_max = max(dx);
dx_min = min(dx);

%dxs = sort(dx+(10^-6)*randn(size(dx)));
%pp = 1 - (1:h.n)/h.n;
%Proba_tmp = interp1(dxs,pp,target_thres_vec,'spline',0);
%Proba = Proba_tmp(I);

% I = find(target_thres_vec>=dx_max);
% if ~isempty(I)
%   Proba(I) = mean(dx==dx_max);
% end
% I = find(target_thres_vec<=dx_min);
% if ~isempty(I)
%   Proba(I) = 1-mean(dx==dx_min);
% end

%Ind = find(target_thres_vec<dxs(end));
for ki=1:length(target_thres_vec)
  n_sup = sum(dx>=target_thres_vec(ki));
  Proba(I(ki)) = n_sup/h.n;
  Stat(ki) = stats_estimator_MC(h.n,'iter',n_sup);
end

%% SubFunctions - Default problem
%  SCORE_INTERNAL, GENERATE_INTERNAL

function d = SCORE_INTERNAL(X)
%% SCORE_INTERNAL
%  d = SCORE(X)
% Calculate the score for all the vectors store in matrix X
% the score is here the absolute value of the  normalized correlation
% * X : a vector or a matrix
% * d : a scalar or a vector
d = abs(X(1,:)./sqrt(sum(X.^2,1)));

function X = GENERATE_INTERNAL(dim,n)
%% GENERATE_INTERNAL
%  X = GENERATE(L,n)
% Generate n vectors of length dim
% distributed as p_X (here, white gaussian noise)
X = randn(dim,n);