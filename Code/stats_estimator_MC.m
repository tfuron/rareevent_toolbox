function stat = stats_estimator_MC(n,varargin)
% stat = stats_estimator_MC(n,options)
% Gives some statistical properties of the estimator_MC
%
% Inputs
% * n : number of particles
%
% Options
% * alpha : confidence interval at alpha
%
% * P : probability to be estimated
% OR
% * iter : number of iterations
%
% Outputs
% stat : a struct containing
% * rel_bias : relative bias
% * rel_var : relative variance
% * interval : Interval of confidence
% * NbIter : number of iterations
%
% Comments
% -
% Authors
% * Implementation: Teddy Furon
% * Science: Arnaud Guyader, Nicolas Hengartner, Eric Matzner-Lober
%
% Contact: teddy.furon@inria.fr
%
% Copyright INRIA 2010
%
% Licence
% This software and its subprograms are governed by the CeCILL license
% under French law and abiding by the rules of distribution of free software.
% See http://www.cecill.info/licences.en.html


%% Parsing the arguments
error(nargoutchk(0,3,nargout));

% Create an instance of the inputParser class.
p = inputParser;
% Define inputs that one must pass on every call:
p.addRequired('n', @(x)validateattributes(x,{'numeric'},{'integer', 'positive'}));
p.addParamValue('iter',100,@(x)validateattributes(x,{'numeric'},{'integer', 'positive'}));
p.addParamValue('P',10^-7,@(x)validateattributes(x,{'numeric'},{'scalar'}));
p.addParamValue('alpha',0.95,@(x)validateattributes(x,{'numeric'},{'positive','scalar'}));

% Parse and validate all input arguments.
p.StructExpand = true;
p.KeepUnmatched = true;
p.parse(n,varargin{:});
defaulted = p.UsingDefaults;
h = p.Results;

%% Which mode?
if nnz(strcmp('P',defaulted))>0
    %% Operational mode
    % 'P' was not passed
    % a real estimator was runned before
    n0 = h.iter;
    Prob = n0/h.n;
else
    %% Theoretical mode
    % 'P' was passed
    % We want an estimate of the properties for such a probability to be
    % estimated
    n0 = floor(h.n*h.P);
    Prob = h.P;
end
%% Statistical properties
% Number of iteration
stat.NbIter = n0;

if n0==0
    stat.rel_bias = NaN;
    stat.rel_var = NaN;
else
    stat.rel_bias = 0; % relative bias of the estimation
    stat.rel_var = (1-Prob)/h.n/Prob; % relative variance of the estimation
end

% Interval of alpha% confidence
% with Agresti-Coull Interval
% see http://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval
La = sqrt(2)*erfinv(h.alpha);
ntilde = h.n+La^2;
ptilde = (n0+La^2/2)/ntilde;
stat.interval = ptilde+[-1;1]*La*sqrt(ptilde*(1-ptilde))/sqrt(ntilde);
stat.interval(1) = max([stat.interval(1) 0]);
stat.interval(2) = min([stat.interval(2) 1]);
% Order fields
stat = orderfields(stat);
